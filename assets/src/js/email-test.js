let emailTest = (function ( document, window ) {

    "use strict";

    //
    // Variables
    //

    var publicAPIs = {};

    //
    // Public Methods
    //

    /**
     * Initialization
     */
    publicAPIs.init = function () {
        register_events();
    };

    //
    // Private Methods
    //

    /**
     * Register events.
     */
    let register_events = function() {
        document.addEventListener( "click", function( event ) {
            let target = event.target;

            if ( target.matches( "[data-selector='send-email-button']" ) ) {
                event.preventDefault();
                sendEmail();
            }
        });
    };

    /**
     * Send email.
     */
    let sendEmail = function() {
        const data = new FormData();
        data.append( "action", "gt_smtp_send_email");
        data.append( "nonce", emailTestAjax.nonce); // eslint-disable-line no-undef

        const emailTo =
            document.querySelector("[data-selector='send-email-to']");
        if ( emailTo ) {
            data.append( "emailTo", emailTo.value);
        }

        let url = window.ajaxurl;

        fetch( url, {
            method: "POST",
            body: data
        } ).then( function( response ) {
            if ( response.ok ) {
                return response.json();
            }

            return Promise.reject( response );
        } ).then( function (data ) {
            let html = JSON.parse( data );
            displayNotification( html );
        }).catch( function( error ) {
            console.error( error );
        } );
    };

    /**
     * Displays a notification.
     *
     * @param {string} html The html.
     */
    let displayNotification = function( html ) {
        let heading =
            document.querySelector("[data-selector='send-email-heading']");
        if ( heading ) {
            heading.insertAdjacentHTML( "beforebegin", html);
        }
    };

    return publicAPIs;
})( document, window );

emailTest.init();