# GT SMTP

GtSmtp is a WordPress plugin for sending emails via SMTP instead of the default PHPMailer.

## Requirements

- Composer version 2+ [https://getcomposer.org/](https://getcomposer.org/)

## Installation

Installing this plugin assumes you are managing plugins for a WordPress site with `composer` or `wp-packagist`. It is currently not part of the WordPress plugin repository so it can't be installed with `wp-packagist`.

Add the following configuration to your `composer.json`. If you are managing plugins with `wp-packagist`, this configuration may already be present.

```json
    "extra": {
        "installer-paths": {
            "plugins/{$name}/": ["type:wordpress-plugin"]
        }
    }
```

Install with composer

```bash
composer require grofftech/gt-smtp
```

Activate the plugin from the WordPress administration area. Once the plugin is successfully activated the `smtp-config.php` file will be automatically created in `wp-content/gt-smtp-config`. Update this file with the relevant SMTP server settings.

From the WordPress administration area, click on the GT SMTP from the menu. Verify the settings added in `smtp-config.php` display. Note that the password will not display here.

## Testing

From the WordPress administration area, navigate to `GT SMTP > Email Test`. Enter in a valid email address in the Email To field and click Send Email.

## Contributing

Pull requests welcome. Please open an issue so we can discuss.

## License

GNU General Public License 3.0+
