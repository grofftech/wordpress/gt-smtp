##################
# Executables
##################
SASS=node_modules/.bin/sass
POST_CSS=node_modules/.bin/postcss
STYLELINT=node_modules/.bin/stylelint
WATCH=node_modules/.bin/watch
TERSER=node_modules/.bin/terser
ESLINT=node_modules/.bin/eslint
BROWSERSYNC=node_modules/.bin/browser-sync
SVGO=node_modules/.bin/svgo
SVGSTORE=node_modules/.bin/svgstore
IMAGEMIN=node_modules/.bin/imagemin

##################
# Configuration
##################
CONFIG_DIR=config
STYLELINT_CONFIG=$(CONFIG_DIR)/stylelintrc.json
STYLELINT_IGNORE=$(CONFIG_DIR)/.stylelintignore
ESLINT_CONFIG=$(CONFIG_DIR)/.eslintrc.json
ESLINT_IGNORE=$(CONFIG_DIR)/.eslintignore
BROWSERSYNC_CONFIG=$(CONFIG_DIR)/browser-sync-config.js
SVGO_CONFIG=$(CONFIG_DIR)/svgo.config.js

##################
# Sources
##################
SOURCE_DIR=assets/src

#######
# CSS
#######
CSS_SOURCE_DIR=$(SOURCE_DIR)/sass
SASS_FILE=$(SOURCE_DIR)/sass/style.scss
SASS_FILES=$(SOURCE_DIR)/sass/modules/*

# List of SASS files to compile (space separated)
SASS_INDIVIDUAL_FILES="$(CSS_SOURCE_DIR)/email-test.scss"

#######
# JS
#######
JS_SOURCE_DIR=$(SOURCE_DIR)/js

# List of JavaScript files to compile (space separated)
JS_FILES="$(JS_SOURCE_DIR)/email-test.js"

#######
# SVG
#######
SVG_SOURCE_DIR=$(SOURCE_DIR)/svg

#########
# Images
#########
IMAGE_FILES_JPEG=$(SOURCE_DIR)/images/*.jpg
IMAGE_FILES_PNG=$(SOURCE_DIR)/images/*.png

#######################
# Distribution / Build
#######################
DIST_DIR=assets/dist

#######
# CSS
#######
BUILD_CSS_DIR=$(DIST_DIR)/css

BUILD_CSS_FILE=$(BUILD_CSS_DIR)/style.css
BUILD_CSS_FILE_MIN=$(BUILD_CSS_DIR)/style.min.css

#######
# JS
#######
BUILD_JS_DIR=$(DIST_DIR)/js

#######
# SVG
#######
BUILD_SVG_DIR=$(DIST_DIR)/svg
BUILD_SVG_OPTIMIZED_DIR=$(BUILD_SVG_DIR)/optimized
BUILD_SVG_FILES=$(BUILD_SVG_OPTIMIZED_DIR)/*.svg
BUILD_SVG_SPRITE_FILE=$(BUILD_SVG_DIR)/sprite.svg

#########
# Images
#########
BUILD_IMAGES_DIR=$(DIST_DIR)/images

############
# Languages
############
BUILD_LANGUAGES_DIR=$(DIST_DIR)/languages


#######################
# Tasks
#######################
all: clean $(BUILD_CSS_FILE) js-files

#######
# CSS
#######

# Build the CSS styles
$(BUILD_CSS_FILE): clean $(BUILD_CSS_DIR) compile-style minimize-style

# Creates the directory for the final CSS
$(BUILD_CSS_DIR):
	@ echo "Creating directory '$(BUILD_CSS_DIR)'..."
	@ mkdir -p $@

# Compiles sass files
compile-style: lint-sass
	@ echo "Compiling Sass for '$(SASS_FILE)'..."
	@ $(SASS) $(SASS_FILE) $(BUILD_CSS_FILE) \
		--style=expanded \
		--source-map \
		--quiet
	@ echo "Compiling individual Sass files..."
	@./scripts/compile-sass-files.sh $(SASS_INDIVIDUAL_FILES) $(BUILD_CSS_DIR)

# Lints sass files
lint-sass:
	@ echo "Linting Sass..."
	@ $(STYLELINT) $(SASS_FILES) \
		--ignore-path $(STYLELINT_IGNORE) \
		--config $(STYLELINT_CONFIG)

# Minimizes styles
minimize-style: autoprefix-style
	@ echo "Minifying '$(BUILD_CSS_FILE)'..."
	@ $(POST_CSS) \
		-u cssnano \
		-o $(BUILD_CSS_FILE_MIN) \
		$(BUILD_CSS_FILE)

# Adds any vendor prefixes based on browserslist configuration in package.json
autoprefix-style:
	@ echo "Autoprefixing '$(BUILD_CSS_FILE)'..."
	@ $(POST_CSS) \
		-u autoprefixer \
		-r $(BUILD_CSS_FILE)

#######
# JS
#######

# Compiles JavaScript files
js-files: lint-js $(BUILD_JS_DIR)
	@echo "Compiling JS Files..."
	@./scripts/compile-js-files.sh $(JS_FILES) $(BUILD_JS_DIR)
	@./scripts/compile-js.sh

# Creates the directory for the final JavaScript
$(BUILD_JS_DIR):
	@ echo "Creating directory '$(BUILD_JS_DIR)'..."
	@ mkdir -p $@

# Checks JavaScript formatting
lint-js:
	@ echo "Linting JS..."
	@ $(ESLINT) \
		--config $(ESLINT_CONFIG) \
		--ignore-path $(ESLINT_IGNORE) \
		$(JS_SOURCE_DIR)

#######
# SVG
#######

# Optimizies and compiles individual SVGs into a sprite
svg-files:
	@ echo "Processing SVGs..."
	@ echo "Creating directory '$(BUILD_SVG_DIR)'..."
	@ mkdir $(BUILD_SVG_DIR)
	@ echo "Creating directory '$(BUILD_SVG_OPTIMIZED_DIR)'..."
	@ mkdir $(BUILD_SVG_OPTIMIZED_DIR)
	@ echo "Optimizing and compiling..."
	@ $(SVGO) \
		-q \
		--config $(SVGO_CONFIG) \
		-f $(SVG_SOURCE_DIR) \
		-o $(BUILD_SVG_OPTIMIZED_DIR) \
	  && \
	  	$(SVGSTORE) \
	  		-o $(BUILD_SVG_SPRITE_FILE) \
			$(BUILD_SVG_FILES)

########
# Images
########

# Optimizes all .jpg images
images-jpg:
	@ echo "Minifying JPEG images..."
	@ $(IMAGEMIN) \
		$(IMAGE_FILES_JPEG) \
		-o $(BUILD_IMAGES_DIR) \
		-p.mozjpeg.quality=80

# Optimizies all .png images
images-png:
	@ echo "Minifying PNG images..."
	@ $(IMAGEMIN) \
		$(IMAGE_FILES_PNG) \
		-o $(BUILD_IMAGES_DIR)

#######
# Other
#######

# Runs browsersync
browser-sync:
	@ $(BROWSERSYNC) start --config $(BROWSERSYNC_CONFIG)

# Cleans the distribution directory
clean:
	@ echo "Removing directory '$(DIST_DIR)'..."
	@ rm -rf $(DIST_DIR)

# Run the all task and watches for changes
watch: clean
	@./watch

.PHONY: all compile-style lint-sass minimize-style autoprefix-style js-files lint-js svg-files images-jpeg images-png browser-sync clean watch
