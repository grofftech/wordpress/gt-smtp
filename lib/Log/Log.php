<?php
/**
 * Log Handler
 *
 * @package     Grofftech\GtSmtp\Log
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Log;

use Throwable;

/**
 * Log class
 */
class Log {

    private static  $root_dir = GT_SMTP_DIR;

	/**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Runs any class setup code besides constructor
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {

    }

    /**
     * Logs a message.
     *
     * @since 1.0.0
     *
     * @param string $message The message to log.
     *
     * @return void
     */
    public static function log_message( string $message ) {
        self::create_log_file();
        error_log( $message, 3, self::$root_dir . '/error.log'  );
    }

    /**
     * Logs an exception to the debug log.
     *
     * @since 1.0.0
     *
     * @param \Throwable $ex The exception to log.
     *
     * @return void
     */
    public static function log_exception( \Throwable $ex ) {
        // TODO: Add logging any previous exceptions.

        self::create_log_file();
        $message = "\n\n";

        $exception_message = $ex->getMessage();
        $stack_trace = $ex->getTraceAsString();

        if ( '' !== $exception_message ) {
            $message .= 'Message: ';
            $message .= $exception_message . "\n";
        }

        if ( '' !== $stack_trace ) {
            $message .= 'Stack Trace: ' . "\n";
            $message .= $stack_trace;
        }

        $message .= "\n";

        error_log( $message, 3, self::$root_dir . '/error.log'  );
    }

    private static function create_log_file() {
        $log_file = dirname( __DIR__, 1 ) . '/error.log';

        if ( file_exists( $log_file ) ) {
            return;
        }

        fopen( $log_file, 'w' );
        chmod( $log_file, 0644);
    }
}
