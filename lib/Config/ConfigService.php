<?php

/**
 * Config Service
 *
 * @package     Grofftech\GtSmtp\ConfigService
 * @since       1.0.0
 * @author      groffTECH
 * @license     GNU-2.0+
 */

namespace Grofftech\GtSmtp\Config;

use Grofftech\GtSmtp\Utilities\StringHelper;
use Grofftech\GtSmtp\Service\Service;

class ConfigService extends Service {

    /**
     * Configurations.
     *
     * @var array
     */
    private $configurations = array();

    /**
     * Constructor.
     */
    public function __construct() {

    }

    /**
     * Registers WordPress hooks.
     */
    public function register_hooks() {

    }

    /**
    * Gets all the configurations.
    *
    * @since 1.0.0
    *
    * @return array Returns an array of configurations.
    */
    public function get_configurations() {
        return $this->configurations;
    }

    /**
    * Gets all the keys from configurations.
    *
    * @since 1.0.0
    *
    * @return array An array of keys.
    */
    public function get_configuration_keys() {
        return array_keys( $this->configurations );
    }

    /**
    * Gets the keys from configurations
    * starting with a specific word (e.g.) metabox.portfolio
    *
    * @since 1.0.0
    *
    * @param string $starts_with The word the key starts with, including the period.
    *
    * @return array
    */
    public function get_configuration_keys_starting_with( $starts_with ) {
        return array_filter(
            $this->get_configuration_keys(),
            function( $key ) use ( $starts_with ) {
                return StringHelper::starts_with( $key, $starts_with );
            }
        );
    }

    /**
    * Gets a specific configuration.
    *
    * @since 1.0.0
    *
    * @param string $key The configuration key.
    *
    * @return array An array of configuration options.
    */
    public function get_configuration( $key ) {
        return $this->configurations[$key];
    }

    /**
    * Gets configuration options for a specific parameter for a configuration.
    *
    * @since 1.0.0
    *
    * @param string $key The configuration key.
    * @param string $parameter_key The parameter key.
    *
    * @return array An array of configuration options for the parameter.
    */
    public function get_configuration_parameters($key, $parameter_key) {
        $config = $this->get_configuration( $key );

        if ( ! array_key_exists( $parameter_key, $config ) ) {
            throw new \Exception(
                "Configuration for {$parameter_key} does not exist for {$key}"
            );
        }

        return $config[$parameter_key];
    }

    /**
    * Loads a configuration file and saves it.
    * Merges in a default configuration array if supplied.
    *
    * @since 1.0.0
    *
    * @param string $path_to_file The file path of the configuration file to load.
    * @param array $defaults Optional. An array of default configurations.

    * @return void
    */
    public function load_configuration( $path_to_file, array $defaults = array() ) {
        list( $key, $config ) =
            $this->load_configuration_from_filesystem( $path_to_file );

        if ( $defaults ) {
            $config = $this->merge_with_defaults(
                $config,
                $defaults
            );
        }

        if ( array_key_exists('custom_fields', $config ) ) {
            $config = $this->clean_up_custom_fields_configuration( $config );
        }

        $this->save_configuration( $config, $key );
    }

    /**
    * Loads the configuration file from the filesystem
    *
    * @since 1.0.0
    *
    * @param string $path_to_file The file path of the configuration file to load.
    *
    * @return array The array key and the first element in the array.
    */
    private function load_configuration_from_filesystem( $path_to_file ) {
        $config = require $path_to_file;

        return array(
            key( $config ),
            current( $config )
        );
    }

    /**
    * Saves the configuration.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function save_configuration( $config_to_store, $key ) {
        $this->configurations[$key] = $config_to_store;
    }

    /**
    * Merges the configuration from the store with default configurations.
    *
    * @since 1.0.0
    *
    * @param array $config An array of configuration options.
    * @param array $defaults An array of default configuration options.

    * @return array A new array with merged configuration options.
    */
    private function merge_with_defaults( array $config, array $defaults ) {
        return array_replace_recursive( $defaults, $config );
    }

    /**
    * Removes the custom fields key from the the default configuration
    * and then merges the default options back in, so custom configurations
    * have the options they need.
    *
    * @since 1.0.0
    *
    * @param array $config The configuration.
    *
    * @return array A merged array of custom fields options.
    */
    private function clean_up_custom_fields_configuration( array $config ) {
        $custom_fields = $config['custom_fields'];
        $defaults = array_shift( $custom_fields );

        foreach( $custom_fields as $key => $value ) {
            $custom_fields[$key] = array_merge($defaults, $value);
        }

        $config['custom_fields'] = $custom_fields;

        return $config;
    }
}