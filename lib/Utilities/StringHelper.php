<?php

/**
 * String Helpers
 *
 * @package     Grofftech\GtSmtp\StringHelpers
 * @since       1.0.0
 * @author      groffTECH
 * @license     GNU-2.0+
 */

namespace Grofftech\GtSmtp\Utilities;

class StringHelper {
    /**
    * Performs a search on a string starting with another string.
    *
    * @since 1.0.0
    *
    * @param string $haystack The string to search.
    * @param string $needle The string to search for.
    * @param string $encoding The encoding, optional. Default is UTF-8.
    *
    * @return bool True if the string being searched starts with the string searching for, otherwise false.
    */
    public static function starts_with( $haystack, $needle, $encoding = 'UTF-8' )
    {
        $needle_length = mb_strlen( $needle, $encoding );
        return mb_substr( $haystack, 0, $needle_length, $encoding ) === $needle;
    }

    /**
     * Gets the position of the first occurrence of a string in a string
     *
     * @since 1.0.0
     *
     * @param string $string_to_search The string to perform the search on.
     * @param string $string_to_find   The string to find in the search string.
     * @param string $occurrence       The occurrence to find (first, last).
     * @param int    $start_offset     The position offset to start the search. If
     *                                 negative, starts the search from the end.
     * @param string $encoding         The encoding to use, default is UTF-8.
     *
     * @return bool|false|int|string
     */
    public static function get_string_position( string $string_to_search, string $string_to_find, $occurrence = 'first', $start_offset = 0, $encoding = 'UTF-8' ) {
        if ( 'first' === $occurrence ) {
            return mb_strpos(
                $string_to_search,
                $string_to_find,
                $start_offset,
                $encoding
            );
        }

        if ( 'last' === $occurrence ) {
            return mb_strrpos(
                $string_to_search,
                $string_to_find,
                $start_offset,
                $encoding
            );
        }
    }
}