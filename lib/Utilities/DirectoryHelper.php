<?php
/**
 * Directory Helper
 *
 * @package     Grofftech\GtSmtp\Utilities
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Utilities;

class DirectoryHelper {
    /**
     * Create a directory
     *
     * @since 1.0.0
     *
     * @param string $directory The directory path.
     *
     * @return void
     */
    public static function create_directory( $directory ) {
        if ( ! file_exists( $directory ) ) {
            mkdir( $directory );
        }
    }

    /**
     * Remove a directory.
     *
     * @since 1.0.0
     *
     * @param string $directory The directory path.
     *
     * @return void
     */
    public static function remove_directory( $directory ) {
        foreach ( glob( $directory . '/*' ) as $file ) {
            if ( is_dir( $file ) ) {
                self::remove_directory( $file );
            }
            else {
                unlink( $file );
            }
        }

        rmdir( $directory );
    }
}
