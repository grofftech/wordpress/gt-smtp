<div class="wrap">
    <h2 data-selector="send-email-heading">Email Test</h2>

    <form action="" method="post">
        <div class="send-email__fields">
            <label for="sendEmail">Email To</label>
            <input type="text" id="sendEmail" name="sendEmail" data-selector="send-email-to" />
        </div>

        <div class="send-email__button">
            <button type="submit" class="button button-primary" data-selector="send-email-button">Send Email</button>
        </div>
    </form>
</div>