<input
    type="checkbox"
    id="<?php esc_attr_e( $option_id ) ?>"
    name="<?php esc_attr_e( $option_name ) ?>"
    value="1"
    class="<?php esc_attr_e( $classes ) ?>"
<?php esc_attr_e( $checked ); ?>
 <?php esc_attr_e( $disabled ) ?> />

<?php if ( '' !== $args['help_text'] ): ?>
    <p>
        <?php esc_html_e( $args['help_text'] )?>
    </p>
<?php endif; ?>