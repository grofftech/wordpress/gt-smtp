<input id="<?php esc_attr_e( $option_id ); ?>"
       type="text"
       name="<?php esc_attr_e( $option_name ); ?>"
       value="<?php esc_attr_e( $option_value ); ?>"
       class="<?php esc_attr_e( $classes ) ?>"
       <?php esc_attr_e( $disabled ) ?>
/>

<?php if ( '' !== $args['help_text'] ): ?>
    <p>
        <?php esc_html_e( $args['help_text'] )?>
    </p>
<?php endif; ?>