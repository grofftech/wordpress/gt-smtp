<h2><?php echo $page_title ?></h2>

<form action="options.php" method="post">
    <?php
    settings_fields( $options_group );
    do_settings_sections( $page );
    ?>
    <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
</form>