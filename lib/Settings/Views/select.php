<select
    id="<?php esc_attr_e( $option_id ); ?>"
    name="<?php esc_attr_e( $option_name ); ?>"
    class="<?php esc_attr_e( $classes ) ?>"
    <?php esc_attr_e( $disabled ) ?>
>
    <?php for ( $i = 0; $i < count( $select_options ); $i++) { ?>
        <?php
            $selected = '';
            if ( isset( $options[$option_id] ) ) {
                $selected = selected( $options[$option_id], $i + 1, false );
            }
        ?>
        <option value="<?php echo $i + 1 ?>" <?php esc_attr_e( $selected ) ?>>
            <?php esc_html_e( $select_options[$i] ); ?>
        </option>
    <?php } ?>
</select>

<?php if ( '' !== $args['help_text'] ): ?>
    <p>
        <?php esc_html_e( $args['help_text'] )?>
    </p>
<?php endif; ?>