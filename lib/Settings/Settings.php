<?php
/**
 * Plugin settings handler.
 *
 * @package     Grofftech\GtSmtp\Settings
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Settings;

use Exception;
use Grofftech\GtSmtp\Config\ConfigService;
use Grofftech\GtSmtp\Service\Service;
use Grofftech\GtSmtp\Settings\Validation\RequiredValidator;
use Grofftech\GtSmtp\Utilities\StringHelper;

/**
 * Settings class.
 */
class Settings extends Service {

    /**
     * Config Service.
     *
     * @var ConfigService
     */
    private $config_service;

    /**
     * Configuration Files.
     *
     * @var array
     */
    private $configuration_files = array(
        __DIR__ . '/Config/MainPage.php',
        __DIR__ . '/Config/EmailTest.php',
    );

    /**
     * Loaded configurations.
     *
     * @var array
     */
    private $configurations = array();

    /**
     * Constructor.
     *
     * @param ConfigService $config_service The config service object.
     *
     */
    public function __construct( ConfigService $config_service ) {
        $this->config_service = $config_service;
    }

    /**
     * Initialize/Run the code for the class.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->get_configuration();
        $this->register_hooks();
    }

    private function get_configuration() {
        $defaults =
            require __DIR__ . '/Config/Defaults.php';
        $defaults = current( $defaults );

        foreach ( $this->configuration_files as $file ) {
            $this->config_service->load_configuration( $file, $defaults );
        }

        $configuration_keys =
            $this->config_service
                 ->get_configuration_keys_starting_with('settings');

        foreach ( $configuration_keys as $configuration_key ) {
            array_push(
                $this->configurations,
                $this->config_service->get_configuration( $configuration_key )
            );
        }
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action(
            'admin_init',
            array( $this, 'register_settings' )
        );

        \add_action(
            'admin_menu',
            array( $this, 'create_settings_menu' )
        );

        \add_action(
            'admin_notices',
            array( $this, 'display_notices' )
        );
    }

    /**
     * Create the settings menu.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function create_settings_menu() {
        foreach ( $this->configurations as $config ) {
            if ( 'main' === $config['menu']['menu_type'] ) {
                \add_menu_page(
                    $config['menu']['page_title'],
                    $config['menu']['menu_title'],
                    $config['menu']['capabilities'],
                    $config['menu']['menu_slug'],
                    array( $this, $config['menu']['render_function'] ),
                    $config['menu']['icon_url']
                );
            }

            if ( 'sub' === $config['menu']['menu_type'] ) {
                \add_submenu_page(
                    $config['menu']['submenu']['parent_menu_slug'],
                    $config['menu']['page_title'],
                    $config['menu']['menu_title'],
                    $config['menu']['capabilities'],
                    $config['menu']['menu_slug'],
                    array( $this, $config['menu']['render_function'] )
                );
            }
        }
    }

    /**
     * Renders the settings page.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function render_settings_page() {
        $page = $this->get_page_slug();

        $config = $this->find_configuration_by_page();
        $options_group = $config['register']['option_group'];
        $page_title = $config['menu']['page_title'];

        include __DIR__ . '/Views/settings.php';
    }

    /**
     * Registers settings.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_settings() {
        foreach ( $this->configurations as $config ) {
            $this->register_setting( $config );
            $sections = $config['sections'];

            foreach ( $sections as $section ) {
                $this->add_fields( $section, $config );
            }
        }
    }

    private function register_setting( $config ) {
        if ( isset($config['register']['option_group'] ) ) {
            \register_setting(
                $config['register']['option_group'],
                $config['register']['option_name'],
            array(
                'sanitize_callback' => array(
                        $this,
                        $config['register']['sanitize_function'],
                )
            )
            );
        }
    }

    private function add_fields( $section, $config  ) {
        \add_settings_section(
            $section['id'],
            $section['title'],
            array( $this, $section['render_function'] ),
            $config['menu']['menu_slug'],
        );

        $fields = $section['section_fields']['fields'];

        // Sort by priority.
        usort( $fields, function( $item1, $item2 ) {
            return $item1['priority'] <=> $item2['priority'];
        });

        foreach( $fields as $field ) {
            $render_function = '';

            if ( 'text' === $field['type'] ) {
                $render_function =
                    $section['section_fields']['render_function_text'];
            }

            if ( 'checkbox' === $field['type'] ) {
                $render_function =
                    $section['section_fields']['render_function_checkbox'];
            }

            if ( 'select' === $field['type'] ) {
                $render_function =
                    $section['section_fields']['render_function_select'];
            }

            \add_settings_field(
                $field['id'],
                $field['title'],
                array( $this, $render_function ),
                $config['menu']['menu_slug'],
                $section['name'],
                $field['args'],
            );
        }
    }

    /**
     * Validates the settings.
     *
     * @since 1.0.0
     *
     * @param array $input An array of id's for all the settings.
     *
     * @return bool|false|int|string
     */
    public function validate_settings( $input ) {
        if ( null === $input ) {
            return;
        }

        $new_input = array();
        $config = $this->find_configuration_by_page();

        foreach ( $input as $field => $value ) {
            foreach ( $config['sections'] as $section ) {
                $fields = $section['section_fields']['fields'];

                if ( array_key_exists( $field, $fields ) ) {
                    $field_config = $fields[$field];

                    $required = isset( $field_config['required'] )
                        ? $field_config['required']
                        : false;

                    $custom_validation =
                        isset( $field_config['custom_validation'] )
                            ? $field_config['custom_validation']
                            : false;

                    // Required but no validation
                    if ( 'true' === $required
                         && 'false' === $custom_validation ) {

                        $validator =
                            new RequiredValidator(
                                $field_config['id'],
                                $config['menu']['menu_slug'],
                                $field_config['title']
                            );

                        if ( $validator->is_valid( $value ) ) {
                            $new_input[$field] = \sanitize_text_field( $value);
                        }
                    }
                    // Required with custom validation
                    else if ( 'true' === $required
                              && 'true' === $custom_validation ) {
                        $class = $field_config['custom_validation_class'];

                        if ( isset( $class ) ) {
                            $validator = new $class();

                            // Must inherit from abstract Input Validator
                            // and provide overrides
                            if ( $validator->is_valid( $value ) ) {
                                $new_input[$field] =
                                    \sanitize_text_field( $value);
                            }
                        }
                        else {
                            throw new Exception(
                                'The custom_validation_class config setting must be provided'
                            );
                        }
                    }
                    // Sanitize
                    else {
                        $new_input[$field] = \sanitize_text_field( $value );
                    }
                }
            }
        }

        return $new_input;
    }

    /**
     * Renders markup for a section.
     *
     * @since 1.0.0
     *
     * @param array $args The args related to the section.
     *
     * @return void
     */
    public function render_section( $args ) {
        if ( empty( $args['id'] ) ) {
            return;
        }

        include __DIR__ . '/Views/' . $args['id'] . '.php';
    }

    /**
     * Renders a text field.
     *
     * @since 1.0.0
     *
     * @param array $args The args related to the text field.
     *
     * @return void
     */
    public function render_text_field( $args ) {
        if ( ! isset( $args['id'] ) ) {
            throw new \Exception( 'The id key needs to be set in the args config of the text field' );
        }

        if ( ! isset( $args['type'] ) ) {
            throw new \Exception( 'The type key needs to be set in the args config of the text field' );
        }

        if ( ! isset( $args['disabled'] ) ) {
            throw new \Exception( 'The disabled key needs to be set in the args config of the text field' );
        }

        $config = $this->find_configuration_by_page();
        $options_group = $config['register']['option_group'];
        $options = \get_option( $options_group );

        $option_id = $args['id'];
        $option_name = "{$options_group}[{$option_id}]";
        $option_value = '';

        if ( key_exists( $option_id, (array) $options ) ) {
            $option_value = $options[$option_id];
        }
        else {
            $option_value = constant( strtoupper( $args['id'] ) );
        }

        $disabled = 'true' === $args['disabled']
            ? 'disabled'
            : '';

        $type = $args['type'];

        $classes = $this->add_classes( $args['class'], $option_id );

        include __DIR__.'/Views/text-field.php';
    }

    /**
     * Renders a checkbox.
     *
     * @since 1.0.0
     *
     * @param array $args The args related to the checkbox.
     *
     * @return void
     */
    public function render_checkbox( $args ) {
        if ( ! isset( $args['id'] ) ) {
            throw new Exception( 'The id needs to be set in the args config of the checkbox field' );
        }

        if ( ! isset( $args['disabled'] ) ) {
            throw new \Exception( 'The disabled key needs to be set in the args config of the text field' );
        }

        $config = $this->find_configuration_by_page();
        $options_group = $config['register']['option_group'];
        $options = \get_option( $options_group );

        $option_id = $args['id'];
        $option_name = "{$options_group}[{$option_id}]";

        $checked = '';
        if ( isset( $options[$option_id] ) ) {
            $checked = checked( $options[$option_id], 1, false );
        }
        else {
            $checked = constant( strtoupper( $args['id'] ) ) === 'true'
                ? 'checked'
                : '';
        }

        $disabled = 'true' === $args['disabled']
            ? 'disabled'
            : '';

        $classes = $this->add_classes( $args['class'], $option_id );

        include __DIR__.'/Views/checkbox.php';
    }

    /**
     * Renders a select box.
     *
     * @since 1.0.0
     *
     * @param array $args The arguments related to the select.
     *
     * @return void
     */
    public function render_select( $args ) {
        if ( ! isset( $args['id'] ) ) {
            throw new Exception( 'The id needs to be set in the args config of the select field' );
        }

        if ( ! isset( $args['disabled'] ) ) {
            throw new \Exception( 'The disabled key needs to be set in the args config of the text field' );
        }

        $config = $this->find_configuration_by_page();
        $options_group = $config['register']['option_group'];
        $options = \get_option( $options_group );

        $option_id = $args['id'];
        $option_name = "{$options_group}[{$option_id}]";

        // Data population
        $select_options = [];
        if ( false === $args['custom_options'] ) {
            $select_options = $args['options'];
        } else {
            $select_options = $args['custom_options_function']();
        }

        $disabled = 'true' === $args['disabled']
            ? 'disabled'
            : '';

        $classes = $this->add_classes( $args['class'], $option_id );

        include __DIR__ . '/Views/select.php';
    }

    /**
     * Displays notices.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function display_notices() {
        foreach ( $this->configurations as $config ) {
            \settings_errors( $config['menu']['menu_slug'] );
        }
    }

    private function add_classes( string $class, string $id ) {
        $classes = [];

        if ( '' !== $class ) {
            array_push($classes, $class);
        }

        $classes = \apply_filters(
            "plugin_template_settings_{$id}_classes",
            $classes
        );

        return implode( " ", $classes );
    }

    private function find_configuration_by_page() {
        $result = array();

        foreach ( $this->configurations as $config ) {
            $page = $this->get_page_slug();

            if ( in_array( $page, $config['menu'] ) ) {
                $result = $config;
                break;
            }
        }

        return $result;
    }

    private function get_page_slug() {
        $page = '';

        if ( isset( $_GET['page'] ) ) {
            $page = $_GET['page'];
        }

        if ( isset( $_POST['_wp_http_referer'] ) ) {
            $referer = $_POST['_wp_http_referer'];

            $first_equals_position = StringHelper::get_string_position(
                $referer,
                '='
            );

            $first_ampersand_position = StringHelper::get_string_position(
                $referer,
                '&'
            );

            if ( $first_ampersand_position > 0) {
                $page = substr(
                    $referer,
                    $first_equals_position + 1,
                    ($first_ampersand_position - 1) - $first_equals_position
                );
            }

            if ( $first_equals_position > 0
                    && false === $first_ampersand_position) {
                    $page = substr(
                        $referer,
                        $first_equals_position + 1
                    );
            }
        }

        return $page;
    }

    public function render_email_test_page() {
        include __DIR__ . '/Views/EmailTest/email_test.php';
    }
}
