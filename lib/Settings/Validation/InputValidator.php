<?php
/**
 * Validation Base Class
 *
 * @package     Grofftech\GtSmtp\Settings\Validation
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Settings\Validation;

/**
 * Validation class.
 */
abstract class InputValidator {

    /**
     * Setting id.
     *
     * @var string
     */
    private $setting_id;

    /**
     * Menu slug.
     *
     * @var string
     */
    private $menu_slug;

    /**
     * Setting title.
     *
     * @var string
     */
    private $setting_title;

    /**
     * Constructor
     */
    public function __construct( $setting_id, $menu_slug, $setting_title ) {
        $this->setting_id = $setting_id;
        $this->menu_slug = $menu_slug;
        $this->setting_title = $setting_title;
    }

    /**
     * Description
     *
     * @since 1.0.0
     *
     * @return bool|false|int|string
     */
    public function is_valid( string $value ) {
        $is_valid = true;

        if ( '' === trim( $value ) ) {
            $this->add_error( "{$this->setting_title} is required." );
            $is_valid = false;
        }

        return $is_valid;
    }

    private function add_error( string $message ) {
		\add_settings_error(
			$this->menu_slug,
			$this->setting_id,
			$message,
			'error'
		);
    }
}
