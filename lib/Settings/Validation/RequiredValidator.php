<?php
/**
 * REquired validator handler.
 *
 * @package     Grofftech\GtSmtp\Settings\Validation
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */
namespace Grofftech\GtSmtp\Settings\Validation;

/**
 * ClassDescription
 */
class RequiredValidator extends InputValidator {

    /**
     * Constructor
     */
    public function __construct( $id, $menu_slug, $title ) {
        parent::__construct( $id, $menu_slug, $title );
    }
}