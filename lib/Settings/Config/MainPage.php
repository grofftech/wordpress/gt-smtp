<?php
return array(
    'settings.main_page' => array(
        'menu' => array(
            'page_title' => 'GT SMTP Settings',
            'menu_title' => 'GT SMTP',
            'menu_slug' => 'gt-smtp',
            'menu_type' => 'main',
            'submenu' => array(
                'parent_menu_slug' => '',
            ),
            'render_function' => 'render_settings_page',
            'capabilities' => 'manage_options',
            'icon_url' => 'dashicons-email',
        ),
        'register' => array(
            'option_group' => 'gt_smtp_options',
            'option_name' => 'gt_smtp_options',
            'sanitize_function' => 'validate_settings',
        ),
        'sections' => array(
            array(
                'id' => 'main_section',
                'title' => '',
                'name' => 'main_section',
                'render_function' => 'render_section',
                'section_fields' => array(
                    'render_function_text' => 'render_text_field',
                    'render_function_checkbox' => 'render_checkbox',
                    'render_function_select' => 'render_select',
                    'fields' => array(
                        'smtp_user' => array(
                            'type' => 'text',
                            'priority' => 1,
                            'id' => 'smtp_user',
                            'title' => 'SMTP Username',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_user',
                                'type' => 'text',
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true',
                            )
                        ),
                        'smtp_host' => array(
                            'type' => 'text',
                            'priority' => 3,
                            'id' => 'smtp_host',
                            'title' => 'SMTP Host',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_host',
                                'type' => 'text',
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                        'smtp_from_email' => array(
                            'type' => 'text',
                            'priority' => 4,
                            'id' => 'smtp_from_email',
                            'title' => 'From Email Address',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_from_email',
                                'type' => 'text',
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                        'smtp_from_name' => array(
                            'type' => 'text',
                            'priority' => 5,
                            'id' => 'smtp_from_name',
                            'title' => 'SMTP From Name',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_from_name',
                                'type' => 'text',
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                        'smtp_port' => array(
                            'type' => 'text',
                            'priority' => 6,
                            'id' => 'smtp_port',
                            'title' => 'SMTP Port',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_port',
                                'type' => 'text',
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                        'smtp_encryption' => array(
                            'type' => 'select',
                            'priority' => 7,
                            'id' => 'smtp_encryption',
                            'title' => 'SMTP Encryption',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_encryption',
                                'custom_options' => false,
                                'custom_options_function' => '',
                                'options' => array(
                                    'TLS',
                                    'SSL',
                                ),
                                'help_text' => '',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                        'smtp_auth' => array(
                            'type' => 'checkbox',
                            'priority' => 8,
                            'id' => 'smtp_auth',
                            'title' => 'SMTP Authentication',
                            'required' => 'false',
                            'custom_validation' => 'false',
                            'custom_validation_class' => '',
                            'args' => array(
                                'id' => 'smtp_auth',
                                'help_text' => 'Use SMTP for authentication',
                                'class' => '',
                                'disabled' => 'true'
                            )
                        ),
                    )
                ),
            )
        ),
    )
);