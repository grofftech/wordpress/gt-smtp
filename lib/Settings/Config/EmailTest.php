<?php
return array(
    'settings.email_test' => array(
        'menu' => array(
            'page_title' => 'Send a Test Email',
            'menu_title' => 'Email Test',
            'menu_slug' => 'email-test',
            'menu_type' => 'sub',
            'submenu' => array(
                'parent_menu_slug' => 'gt-smtp',
            ),
            'render_function' => 'render_email_test_page',
            'capabilities' => 'manage_options',
            'icon_url' => '',
        ),
    )
);