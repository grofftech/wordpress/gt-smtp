<?php
return array(
    'settings' => array(
        'menu' => array(
            'page_title' => '', // required
            'menu_title' => '', // required
            'menu_slug' => '', // required
            'menu_type' => 'main', // main or sub
            'submenu' => array(
                'parent_menu_slug' => '', // provide if sub
            ),
            'render_function' => 'render_settings_page',
            'capabilities' => 'manage_options',
            'icon_url' => '',
        ),
        'register' => array(
            'option_group' => '', // must be different per page
            'option_name' => '', // must be different per page
            'sanitize_function' => 'validate_settings',
        ),
        'sections' => array(
            array(
                'id' => 'main_section', // required
                'title' => 'Main Section', // optional (will output on page if set)
                'name' => 'main_section', // required
                // A view file with the id name in the Views folder
                // must exist
                'render_function' => 'render_section',
                'section_fields' => array(
                    'render_function_text' => 'render_text_field',
                    'render_function_checkbox' => 'render_checkbox',
                    'render_function_select' => 'render_select',
                    'fields' => array(
                        // 'text_example' => array(
                        //     'type' => 'text',
                        //     'priority' => 0,
                        //     'id' => 'text_example',
                        //     'title' => 'Text Example',
                        //     'required' => 'false',
                        //     'custom_validation' => 'false',
                        //     // if custom_validation is true provide
                        //     // FQDN class name
                        //     // e.g. Company\\Project\\Folder\\Class
                        //     'custom_validation_class' => '',
                        //     'args' => array(
                        //         'id' => 'text_example',
                        //         'type' => 'text'
                        //         'help_text' => '',
                        //         'class' => ''
                        //     )
                        // ),
                        // 'select_example' => array(
                        //     'type' => 'select',
                        //     'priority' => 0,
                        //     'id' => 'select_example',
                        //     'title' => 'Select Example',
                        //     'required' => 'false',
                        //     'custom_validation' => 'false',
                        //     // if custom_validation is true
                        //     // provide FQDN class name
                        //     // e.g. Company\\Project\\Folder\\Class
                        //     'custom_validation_class' => '',
                        //     'args' => array(
                        //         'id' => 'select_example',
                        //         'custom_options' => false,
                        //         'custom_options_function' => '',
                        //         'options' => array(
                        //             'Option 1',
                        //             'Option 2',
                        //             'Option 3',
                        //         ),
                        //         'help_text' => '',
                        //         'class' => ''
                        //     )
                        // ),
                        // 'checkbox_example' => array(
                        //     'type' => 'checkbox',
                        //     'priority' => 1,
                        //     'id' => 'checkbox_example',
                        //     'title' => 'Checkbox Example',
                        //     'required' => 'false',
                        //     'custom_validation' => 'false',
                        //     // if custom_validation is true
                        //     // provide FQDN class name
                        //     // e.g. Company\\Project\\Folder\\Class
                        //     'custom_validation_class' => '',
                        //     'args' => array(
                        //         'id' => 'checkbox_example',
                        //         'help_text' => '',
                        //         'class' => ''
                        //     )
                        // ),
                    )
                ),
            ),
            // Add additional sections as array (copy above array)
        ),
    )
);