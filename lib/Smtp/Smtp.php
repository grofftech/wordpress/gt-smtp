<?php
/**
 * SMTP Handler
 *
 * @package     Grofftech\GtSmtp\Email
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Smtp;

use Exception;
use Grofftech\GtSmtp\Log\Log;
use Grofftech\GtSmtp\Service\Service;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Smtp class.
 */
class Smtp extends Service {
    /**
     * Constructor
     */
    public function __construct() { }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'phpmailer_init', array( $this, 'configure_smtp') );
        \add_action( 'wp_mail_failed', array( $this, 'log_smtp_errors' ) );
    }

    /**
     * Configures SMTP settings.
     *
     * @since 1.0.0
     *
     * @param PHPMailer $mailer The mailer object.
     *
     * @return void
     */
    public function configure_smtp( $mailer ) {
        $mailer->isSMTP();

        $this->validate_constants();

        $mailer->Host = SMTP_HOST;
        $mailer->SMTPAuth = SMTP_AUTH;
        $mailer->Port = SMTP_PORT;
        $mailer->Username = SMTP_USER;
        $mailer->Password = SMTP_PASSWORD;
        $mailer->SMTPSecure = SMTP_ENCRYPTION;
        $mailer->From = SMTP_FROM_EMAIL;
        $mailer->FromName = SMTP_FROM_NAME;
    }

    /**
     * Logs errors for SMTP emailing.
     *
     * @since 1.0.0
     *
     * @param WP_Error $wp_error The error object.
     *
     * @return void
     */
    public function log_smtp_errors( $wp_error ) {
        Log::log_message( $wp_error->get_error_message() );
    }

    private function validate_constants() {
        if ( ! defined( 'SMTP_HOST' ) ) {
            throw new Exception( 'SMTP_HOST constant must be defined' );
        }

        if ( ! defined( 'SMTP_AUTH' ) ) {
            throw new Exception( 'SMTP_AUTH constant must be defined' );
        }

        if ( ! defined( 'SMTP_PORT' ) ) {
            throw new Exception( 'SMTP_PORT constant must be defined' );
        }

        if ( ! defined( 'SMTP_USER' ) ) {
            throw new Exception( 'SMTP_USER constant must be defined' );
        }

        if ( ! defined( 'SMTP_PASSWORD' ) ) {
            throw new Exception( 'SMTP_PASSWORD constant must be defined' );
        }

        if ( ! defined( 'SMTP_ENCRYPTION' ) ) {
            throw new Exception( 'SMTP_ENCRYPTION constant must be defined' );
        }

        if ( ! defined( 'SMTP_FROM_EMAIL' ) ) {
            throw new Exception( 'SMTP_FROM_EMAIL constant must be defined' );
        }

        if ( ! defined( 'SMTP_FROM_NAME' ) ) {
            throw new Exception( 'SMTP_FROM_NAME constant must be defined' );
        }
    }
}
