<?php
/**
 * Registers services required for the plugin.
 *
 * @package     Grofftech\GtSmtp\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Service;

use Grofftech\GtSmtp\Interfaces\Runnable;

/**
 * Abstract service registrar class.
 */
abstract class ServiceRegistrar implements Runnable {

    /**
     * The classes to instantiate
     *
     * @var array
     */
    protected $classes = [];

    /**
     * Run the service registration.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_services();
    }

    /**
     * Register services.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function register_services() {
        $this->classes = $this->init_classes();

        foreach ( $this->classes as $class ) {
            $class->run();
        }
    }

    /**
     * Initialize new classes.
     */
    protected function init_classes() {
        $objects = array_map( function( $class ) {
            return array(
                'namespace' => $class,
                'object' => $this->injector->make( $class )
            );
        }, $this->classes );

        return array_column( $objects, 'object', 'namespace');
    }
}