<?php
/**
 * Service
 *
 * @package     Grofftech\GtSmtp\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Service;

use Grofftech\GtSmtp\Interfaces\Hookable;
use Grofftech\GtSmtp\Interfaces\Runnable;

/**
 * Abstract service class.
 */
abstract class Service implements Runnable, Hookable {

    /**
     * Run.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_hooks();
    }
}