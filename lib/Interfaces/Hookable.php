<?php
/**
 * Hookable Interface
 *
 * @package     Grofftech\GtSmtp\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Interfaces;

/**
 * Hookable interface.
 */
interface Hookable {

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks();
}