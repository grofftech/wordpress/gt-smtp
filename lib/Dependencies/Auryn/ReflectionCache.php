<?php

namespace Grofftech\GtSmtp\Dependencies\Auryn;

interface ReflectionCache
{
    public function fetch($key);
    public function store($key, $data);
}
