<div class="notice notice-success is-dismissible">
    <p><?php echo esc_html( $message ); ?></p>
</div>