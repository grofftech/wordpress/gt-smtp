<div class="<?php echo $classes !== '' ? "{$classes} " : '' ?>notice is-dismissible">
    <p><?php echo esc_html( $message ); ?></p>
</div>