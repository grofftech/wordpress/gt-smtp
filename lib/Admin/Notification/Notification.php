<?php
/**
 * Notification Handler
 *
 * @package     Grofftech\GtSmtp\Admin
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Admin\Notification;

use Grofftech\GtSmtp\Service\Service;

/**
 * Notification class.
 */
class Notification extends Service {
    private $plugin_name = 'gt-smtp';

    /**
     * Constructor.
     */
    public function __construct()
    {

    }

    public function register_hooks() {
        \add_action(
            'admin_notices',
            array( $this, 'render_plugin_activation_message' )
        );
    }

    /**
     * Performs actions to show an error message.
     *
     * @since 1.0.0
     *
     * @param string $message The error message to display.
     *
     * @return void
     */
    public function show_error_message( $message ) {
        // Add a custom action so we can use the message parameter
        \add_action(
            "{$this->plugin_name}-error-message",
            array( $this, 'render_error_message' ),
            10,
            1
        );

        // Do the custom action with the message
		\add_action( 'admin_notices', function() use ( $message ) {
            \do_action( "{$this->plugin_name}-error-message", $message );
        } );
    }

    /**
     * Performs actions to show a notification message.
     *
     * @since 1.0.0
     *
     * @param string $message The notification message to display.
     *
     * @return void
     */
    public function show_notification_message( $message) {
        \add_action(
            "{$this->plugin_name}-notification-message",
            array( $this, 'render_notification_message' ),
            10,
            1
        );

        // Do the custom action with the message
		\add_action( 'admin_notices', function() use ( $message ) {
            \do_action(
                "{$this->plugin_name}-notification-message",
                $message
            );
        } );
    }

    /**
     * Renders error message.
     *
     * @since 1.0.0
     *
     * @param string $message The message to render.
     *
     * @return void
     */
    public function render_error_message( $message ) {
        include __DIR__ . '/Views/Error.php';
    }

    /**
     * Renders notification message.
     *
     * @since 1.0.0
     *
     * @param string $message The message to render.
     *
     * @return void
     */
    public function render_notification_message( $message ) {
        $classes = '';
        include __DIR__ . '/Views/Notification.php';
    }

    /**
     * Get the error notification html as a string.
     *
     * @since 1.0.0
     *
     * @param string $message The message to display.
     *
     * @return string
     */
    public function get_error_notification_html( $message ) {
        ob_start();
        include __DIR__ . '/Views/Error.php';
        return ob_get_clean();
    }

    /**
     * Get the success notification html as a string.
     *
     * @since 1.0.0
     *
     * @param string $message The message to display.
     *
     * @return string
     */
    public function get_success_notification_html( $message ) {
        ob_start();
        include __DIR__ . '/Views/Success.php';
        return ob_get_clean();
    }

    public function render_plugin_activation_message() {
        if ( \get_transient( GT_SMTP_NAME . '-plugin-activation' ) ) {
            $classes = 'updated';
            $message = "The smtp.config file has been automatically created in 'wp-content/gt-smtp-config'. Please update this file with your SMTP server settings.";

            include __DIR__ . '/Views/Notification.php';
            \delete_transient( GT_SMTP_NAME . '-plugin-activation' );
        }
    }
}