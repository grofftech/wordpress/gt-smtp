<?php
/**
 * GtSmtp
 *
 * @package     Grofftech\GtSmtp
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp;

use Grofftech\GtSmtp\Admin\Notification\Notification;
use Grofftech\GtSmtp\Asset\AssetVersioning;
use Grofftech\GtSmtp\Service\ServiceRegistrar;
use Grofftech\GtSmtp\Config\ConfigService;
use Grofftech\GtSmtp\Email\EmailTest;
use Grofftech\GtSmtp\Smtp\Smtp;
use Grofftech\GtSmtp\Log\Log;
use Grofftech\GtSmtp\Settings\Settings;
use Grofftech\GtSmtp\Utilities\DirectoryHelper;

class GtSmtp extends ServiceRegistrar
{
    /**
     * The directory path of the plugin.
     *
     * @var string
     */
    private $plugin_path;

    /**
     * The url of the plugin.
     *
     * @var string
     */
    private $plugin_url;

    /**
     * The Auryn dependency injector.
     *
     * @var Grofftech\GtSmtp\Dependencies\Auryn
     */
    protected $injector;

    /**
     * The suffix for asset files.
     */
    private $asset_suffix;

    /**
     * The Asset Versioning class.
     *
     * @var AssetVersioning
     */
    private $asset_versioning;

    /**
     * Constructor
     *
     * @param string $plugin_path The directory path of the plugin.
     * @param string $plugin_url  The plugin url.
     * @param Grofftech\GtSmtp\Dependencies\Auryn $injector The dependency injector.
     */
    public function __construct( $plugin_path, $plugin_url, $injector )
    {
        $this->plugin_path = $plugin_path;
        $this->plugin_url = $plugin_url;
        $this->injector = $injector;
    }

    /**
     * Classes to be instantiated.
     *
     * @var array
     */
    protected $classes = array(
        ConfigService::class,
        Settings::class,
        Smtp::class,
        Log::class,
        AssetVersioning::class,
        EmailTest::class,
        Notification::class,
    );

    /**
    * Kicks off plugin functionality.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function run() {
        $this->init_constants();
        $this->register_hooks();
        parent::run();

        $this->asset_versioning =
            $this->get_class('Grofftech\GtSmtp\Asset\AssetVersioning');

        $this->asset_suffix = $this->asset_versioning->development_mode
            ? ''
            : '.min';
    }

    /**
    * Initialize plugin constants.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function init_constants()
    {
        if ( ! defined( 'GT_SMTP_DIR' ) ) {
            define( 'GT_SMTP_DIR', $this->plugin_path );
        }

        if ( ! defined( 'GT_SMTP_URL' ) ) {
            define( 'GT_SMTP_URL', $this->plugin_url );
        }

        if ( ! defined( 'GT_SMTP_NAME' ) ) {
            define( 'GT_SMTP_NAME', 'gt-smtp' );
        }

        if ( ! defined( 'GT_SMTP_VERSION' ) ) {
            define( 'GT_SMTP_VERSION', '1.0.0' );
        }

        if ( ! defined( 'GT_SMTP_TEXT_DOMAIN' ) ) {
            define( 'GT_SMTP_TEXT_DOMAIN', 'gt-smtp' );
        }

        if ( ! defined( 'GT_SMTP_CONFIG_DIR_NAME' ) ) {
            define( 'GT_SMTP_CONFIG_DIR_NAME', 'gt-smtp-config' );
        }

        if ( ! defined( 'GT_SMTP_WP_CONTENT' ) ) {
            define( 'GT_SMTP_WP_CONTENT', dirname( __DIR__, 3 ) );
        }

        $config_file =
            GT_SMTP_WP_CONTENT . '/' . GT_SMTP_CONFIG_DIR_NAME . '/smtp-config.php';

        if ( file_exists( $config_file ) ) {
            include $config_file;
        }
    }

    /**
    * Register hooks.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function register_hooks() {
        \add_action(
            'admin_enqueue_scripts',
            array( $this, 'load_admin_assets' ),
            10
        );

        \add_action(
            'wp_enqueue_scripts',
            array( $this, 'load_assets' ),
            10
        );
    }

    /**
     * Loads assets for the admin area (scripts and styles)
     *
     * @since 1.0.0
     *
     * @param string $page The current admin page.
     *
     * @return void
     */
    public function load_admin_assets( $page ) {
        if ( 'gt-smtp_page_email-test' === $page ) {
            $asset_file_js =
                "assets/dist/js/email-test{$this->asset_suffix}.js";

            $asset_file_css =
                "assets/dist/css/email-test{$this->asset_suffix}.css";

            \wp_enqueue_script(
                'email-test',
                GT_SMTP_URL . $asset_file_js,
                array(),
                $this->asset_versioning->get_asset_current_timestamp(
                    GT_SMTP_DIR . $asset_file_js
                ),
                true
            );

            \wp_localize_script(
                'email-test',
                'emailTestAjax',
                array(
                    'nonce' => \wp_create_nonce('emailTest')
                )
            );

            \wp_enqueue_style(
                'email-test',
                GT_SMTP_URL . $asset_file_css,
                array(),
                $this->asset_versioning->get_asset_current_timestamp(
                    GT_SMTP_DIR . $asset_file_css
                )
            );
        }
    }

    /**
     * Loads assets for the front-end (scripts and styles)
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function load_assets() {

    }

    /**
     * Runs anything that needs to be done when
     * the plugin is activated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function activate() {
        if ( ! defined( 'GT_SMTP_NAME' ) ) {
            define( 'GT_SMTP_NAME', 'gt-smtp' );
        }

        if ( ! defined( 'GT_SMTP_CONFIG_DIR_NAME' ) ) {
            define( 'GT_SMTP_CONFIG_DIR_NAME', 'gt-smtp-config' );
        }

        if ( ! defined( 'GT_SMTP_WP_CONTENT' ) ) {
            define( 'GT_SMTP_WP_CONTENT', dirname( __DIR__, 3 ) );
        }

        $config_dir = GT_SMTP_WP_CONTENT . '/' . GT_SMTP_CONFIG_DIR_NAME;
        DirectoryHelper::create_directory( $config_dir );

        $config_file = "{$config_dir}/smtp-config.php";
        $config_file_sample = dirname( __DIR__, 1 ) . '/smtp-config-sample.php';

        if ( ! file_exists( $config_file ) ) {
            set_transient( GT_SMTP_NAME . '-plugin-activation', true, 15 );
            copy( $config_file_sample, $config_file );
        }
        chmod(  $config_file, 0600 );
    }

    /**
     * Runs anything that needs to be done when
     * the plugin is deactivated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function deactivate() {

    }

    /**
     * Runs anything that needs to be done
     * when the plugin is uninstalled.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function uninstall() {
        DirectoryHelper::remove_directory(
            GT_SMTP_WP_CONTENT . '/' . GT_SMTP_CONFIG_DIR_NAME
        );
    }

    /**
     * Gets a specific class instance.
     *
     * @since 1.0.0
     *
     * @param string $classname The namespace with class name.
     *
     * @return object
     */
    public function get_class( string $classname ) {
        return (object) $this->classes[ $classname ];
    }
}