<?php
/**
 * Email Test Handler
 *
 * @package     Grofftech\GtSmtp\Email
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Email;

use Grofftech\GtSmtp\Admin\Notification\Notification;
use Grofftech\GtSmtp\Service\Service;

/**
 * Email Test class.
 */
class EmailTest extends Service {

    /**
     * Notification object.
     *
     * @var Notification
     */
    private $notification;

    /**
     * Constructor
     *
     * @param Notification $notification The notification object.
     */
    public function __construct( Notification $notification ) {
        $this->notification = $notification;
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action(
            'wp_ajax_gt_smtp_send_email',
            array( $this, 'send_email' )
        );
    }

    /**
     * Send an email.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function send_email() {
        check_ajax_referer( 'emailTest', 'nonce');

        $to = $_POST['emailTo'];
        $headers = [];
        $headers[] = 'From: Test <test@test.com>';

        $message = '';
        $notification = '';

        try {
            \wp_mail(
                $to,
                'GT SMTP Test Email',
                'This is a test email using SMTP',
                $headers
            );

            $message = "Email Sent Successfully";
            $notification = json_encode(
                $this->notification->get_success_notification_html( $message )
            );

            \wp_send_json( $notification );

        } catch( \Exception $ex ) {
            Log::log_exception( $ex );

            $message = "Email Failed To Send";
            $notification = json_encode(
                $this->notification->get_error_notification_html( $message )
            );

            \wp_send_json( $notification );
        }
    }
}