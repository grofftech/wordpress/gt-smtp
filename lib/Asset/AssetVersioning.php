<?php
/**
 * Asset Versioning
 *
 * @package     Grofftech\GtSmtp\Asset
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Asset;

use Grofftech\GtSmtp\Service\Service;

/**
 * Asset versioning class.
 */
class AssetVersioning extends Service {

    /**
     * Development Mode.
     *
     * @var bool
     */
    public $development_mode;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->development_mode = $this->is_site_in_development_mode();
    }

    /**
     * Registers hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {

    }

    /**
     * Checks to see if the site is in development mode.
     *
     * @since 1.0.0
     *
     * @return bool True if script debug config setting is set, otherwise false
     */
    private function is_site_in_development_mode()
    {
        if ( ! defined( 'SCRIPT_DEBUG' ) ) {
            return false;
        }

        return constant( 'SCRIPT_DEBUG' );
    }

    /**
     * Get the current timestamp of an asset file.
     *
     * @since 1.0.0
     *
     * @param $asset_file The directory location of the asset file.
     *
     * @return int The timestamp
     */
    public function get_asset_current_timestamp( $asset_file )
    {
        return filemtime( $asset_file );
    }
}