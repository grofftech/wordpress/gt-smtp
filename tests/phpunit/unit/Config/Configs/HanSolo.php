<?php
/**
 * Test Configuration
 *
 * @package     Grofftech\GtSmtp\Tests\Unit\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Tests\Unit\Config;

return array(
    'character.han_solo' => array(
        'details' => array(
            'name' => 'Han Solo',
            'occupation' => 'smuggler',
            'catch_phrase' => 'I\'ve got a bad feeling about this',
        ),
    ),
);
