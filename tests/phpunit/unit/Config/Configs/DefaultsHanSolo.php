<?php
/**
 * Test Configuration
 *
 * @package     Grofftech\GtSmtp\Tests\Unit\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Tests\Unit\Config;

return array(
    'character.name' => array(
        'details' => array(
            'name' => '',
            'occupation' => '',
            'catch_phrase' => 'Aren\'t you a little short for a storm trooper',
        ),
        'hero_status' => false,
    ),
);
