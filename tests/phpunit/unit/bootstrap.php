<?php
/**
 * Unit Test bootstrap.
 *
 * @package     Grofftech\GtSmtp\Tests\Unit
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Tests\Unit;

define( 'GT_SMTP_TESTS_DIRECTORY', __DIR__ );
define(
    'GT_SMTP_PLUGIN_DIRECTORY',
    dirname( dirname( dirname( __DIR__ ) ) )
);

$composer_autoload = GT_SMTP_PLUGIN_DIRECTORY . '/vendor/autoload.php';

if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
}