<?php
/**
 * Test Case for unit tests
 *
 * @package     Grofftech\GtSmtp\Tests\Unit
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Tests\Unit;

use PHPUnit\Framework\TestCase as FrameworkTestCase;
use Brain\Monkey;
use Mockery;

/**
 * Test Case
 */
abstract class TestCase extends FrameworkTestCase {
    /**
	 * Prepares the test environment before each test.
	 */
    protected function setUp(): void {
        parent::setup();
        Monkey\setup();
    }

    /**
	 * Cleans up the test environment after each test.
	 */
    protected function tearDown(): void {
        Monkey\tearDown();
        Mockery::close();
        parent::tearDown();
    }
}
