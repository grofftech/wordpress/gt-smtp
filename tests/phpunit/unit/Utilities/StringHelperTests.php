<?php
/**
 * String Helper Tests
 *
 * @package     Grofftech\GtSmtp\Tests\Unit\Utilities
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\GtSmtp\Tests\Unit\Utilities;

use Grofftech\GtSmtp\Tests\Unit\TestCase;
use Grofftech\GtSmtp\Utilities\StringHelper;

class StringHelperTests extends TestCase {
    /**
	 * Prepares the test environment before each test.
	 */
    protected function setUp(): void {
        parent::setUp();
    }

    /**
	 * Cleans up the test environment after each test.
	 */
    protected function tearDown(): void {
        parent::tearDown();
    }

    /**
     * Test indicates string starts with.
     */
    public function test_should_indicate_string_starts_with() {
        $string = "star wars";
        $search_string = "star";

        // Action
        $result = StringHelper::starts_with( $string, $search_string );

        $this->assertTrue( $result );
    }

    /**
     * Test indicates string does not start with.
     */
    public function test_should_indicate_string_does_not_star_with() {
        $string = "star wars";
        $search_string = "wars";

        // Action
        $result = StringHelper::starts_with( $string, $search_string );

        $this->assertFalse( $result );
    }
}
