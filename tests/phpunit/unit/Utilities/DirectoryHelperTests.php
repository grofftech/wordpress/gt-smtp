<?php
/**
 * Directory Helper Tests
 *
 * @package     Grofftech\GtSmtp\Tests\Unit\Utilities
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

 namespace Grofftech\GtSmtp\Tests\Unit\Utilities;

use Grofftech\GtSmtp\Tests\Unit\TestCase;
use Grofftech\GtSmtp\Utilities\DirectoryHelper;

class DirectoryHelperTests extends TestCase
{
    /**
     * The tests directory path
     *
     * @var constant
     */
    private $tests_directory = GT_SMTP_TESTS_DIRECTORY;

    /**
     * The directory location for testing directories.
     *
     * @var string
     */
    private $test_directory = GT_SMTP_TESTS_DIRECTORY . '/Utilities/Data';

    /**
     * Prepares the test environment before each test.
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Cleans up the test environment after each test.
     */
    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function test_should_delete_an_empty_directory() {
        $dir = $this->test_directory . '/test';
        DirectoryHelper::create_directory( $dir );

        // Action
        DirectoryHelper::remove_directory( $dir );

        $this->assertFalse( file_exists( $dir ) );
    }

    public function test_should_delete_a_non_empty_directory() {
        $dir = $this->test_directory . '/test';

        DirectoryHelper::create_directory( $dir );
        fopen( $dir . '/testing.php', 'w' );

        // Action
        DirectoryHelper::remove_directory( $dir );

        $this->assertFalse( file_exists( $dir ) );
    }

    public function test_should_delete_a_non_empty_directory_with_subfolders() {
        $dir = $this->test_directory . '/test';
        $sub_dir = $dir . '/sub';

        DirectoryHelper::create_directory( $dir );
        fopen( $dir . '/testing.php', 'w' );

        DirectoryHelper::create_directory( $sub_dir );
        fopen( $sub_dir . '/testing_sub.php', 'w' );

        // Action
        DirectoryHelper::remove_directory( $dir );

        $this->assertFalse( file_exists( $dir ) );
    }
}