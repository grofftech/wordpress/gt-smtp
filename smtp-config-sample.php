<?php
/**
 * The mail server name.
 */
define( 'SMTP_HOST', '');

/**
 * Use SMTP Authentication (true or false).
 */
define( 'SMTP_AUTH', '' );

/**
 * The SMTP Port.
 */
define( 'SMTP_PORT', '' );

/**
 * The SMTP username.
 */
define( 'SMTP_USER', '' );

/**
 * The SMTP password.
 */
define( 'SMTP_PASSWORD', '' );

/**
 * The SMTP From Email Address.
 */
define( 'SMTP_FROM_EMAIL', '' );

/**
 * The SMTP From Name.
 */
define( 'SMTP_FROM_NAME', '' );

/**
 * The type of SMTP Encryption (TLS or SSL).
 */
define( 'SMTP_ENCRYPTION', '' );