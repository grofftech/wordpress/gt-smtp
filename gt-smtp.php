<?php
/**
 * Gt Smtp
 *
 * @package     Grofftech\GtSmtp
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @copyright   2016 Grofftech
 * @license     GNU General Public License 2.0+
 *
 * @wordpress-plugin
 * Plugin Name:  GT Smtp
 * Plugin URI:   https://gitlab.com/grofftech/wordpress/gt-smtp
 * Description:  Configures emails in WordPress to use SMTP
 * Version:      1.1.5
 * Author:       Grofftech
 * Author URI:   https://grofftech.net
 * Text Domain:  gt_smtp
 * License:      GPL-2.0+
 * License URI:  http://www.gnu.org/licenses/gpl-3.0.html
 * Requires PHP: 7.4
 */

namespace Grofftech\GtSmtp;

use Grofftech\GtSmtp\Admin\Notification\Notification;
use Grofftech\GtSmtp\Dependencies\Auryn\Injector;

if ( ! defined( 'ABSPATH' ) ) {
    exit( "Not a valid WordPress Installation!" );
}

$plugin_path = \plugin_dir_path( __FILE__ );
$plugin_url = \plugin_dir_url( __FILE__ );

// Check PHP Version
if ( version_compare(PHP_VERSION, '7.4.0' ) <= 0 ) {
    $notification->show_error_message( "GtSmtp requires PHP version 7.4.0 or higher." );

     deactivate_plugin();
}

// Autoload class files.
$composer_autoload = "{$plugin_path}/vendor/autoload.php";

if ( ! file_exists( $composer_autoload ) ) {
    $notification->show_error_message( "Failed to activate GtSmtp. Did you remember to run composer install?" );

    deactivate_plugin();
    return;
}

require $composer_autoload;

// Import and create the notification class
require_once $plugin_path . 'lib/Admin/Notification/Notification.php';
$notification = new Notification();

// Initialize plugin
try {
    global $gt_smtp;

    $plugin_template = new GtSmtp(
        $plugin_path,
        $plugin_url,
        new Injector()
    );

    \add_action(
        'plugins_loaded',
        array( $plugin_template, 'run' )
    );

    \register_activation_hook(
        __FILE__,
        'Grofftech\GtSmtp\GtSmtp::activate'
    );

    \register_deactivation_hook(
        __FILE__,
        'Grofftech\GtSmtp\GtSmtp::deactivate'
    );

    \register_uninstall_hook(
        __FILE__,
        'Grofftech\GtSmtp\GtSmtp::uninstall'
    );
} catch( \Throwable $e) {
    $notification->show_error_message( "Failed to activate the Gt Smtp plugin. Did you remember to run composer install?" );

    deactivate_plugin();
    return;
}

/**
 * Deactivates the plugin.
 *
 * @since 1.0.0
 *
 * @return void
 */
function deactivate_plugin() {
    // This will hide the plugin activation notification
    unset( $_GET['activate'] );

    // Deactivate plugin
    \add_action( 'admin_init', function () {
        \deactivate_plugins( __FILE__ );
	} );
}