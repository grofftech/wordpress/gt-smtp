#!/bin/bash
# Compile Sass Files
# Copyright 2022 groffTECH
SASS=node_modules/.bin/sass
POST_CSS=node_modules/.bin/postcss
SASS_FILES=$1
BUILD_CSS_DIR=$2


if [ -n "$SASS_FILES" ]; then
    for f in $SASS_FILES
    do
        if [ -f "$f" ]; then
            FILENAME_WITH_EXTENSION="${f##*/}"
            FILENAME_WITHOUT_EXTENSION="${FILENAME_WITH_EXTENSION%.*}"

            CSS_FILE_WITHOUT_EXTENSION="$BUILD_CSS_DIR"/"$FILENAME_WITHOUT_EXTENSION"

            # Compile
            echo "Compiling Sass for '$f'..."
	        $SASS "$f" "$CSS_FILE_WITHOUT_EXTENSION".css \
		        --style=expanded \
		        --source-map \
		        --quiet

            # Minify
            echo "Minifying..."
	        $POST_CSS \
		        -u cssnano \
		        -o "$CSS_FILE_WITHOUT_EXTENSION".min.css \
            "$CSS_FILE_WITHOUT_EXTENSION".css

        fi
    done
fi