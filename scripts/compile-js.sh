#!/bin/bash
# Compile JS Modules
# Copyright 2021 groffTECH
#
# This script compiles JavaScript files using rollup.js
#
# It is run in context from the Makefile in the root directory. Running this
# script directly from this folder will not work.
ROLLUP=node_modules/.bin/rollup

if [ -d "assets/src/js/modules" ]; then
    $ROLLUP --config "config/rollup.config.js"
fi